# knockout-api
### a very bad NodeJS forum backend

This is the forums software developed (most hurriedly) for the Knockout forums.  
It's written in NodeJS with TS, and uses MySQL and Redis as the database and cache respectively.  
You really, really shouldn't use this as anything but a toy. It's incomplete, messy, and slow. At best it's a lesson in why "I'll write it quick first and then fix it" doesn't ever work.  
Aqui há dragões.

# License

See LICENSE file.

# Running this project

Requirements:

- Node v10 or later
- Docker
- Docker Compose
- yarn

Instructions:

1. Clone the repo
2. Navigate to `/nashipunch-api`
3. Run a `yarn` (or `npm install` if you promise you won't push npm-related files to the repo)
4. Run `docker-compose up -d`
5. Run `yarn global add sequelize-cli` (or `npm install -g sequelize-cli`) to install the sequelize CLI
6. Run `sequelize db:migrate` to create the tables
7. Run `sequelize db:seed:all` to create the subforums
8. Run `yarn start` (or `npm start`) to fire up your server
9. Open `http://localhost:3000/thread` on your browser to test that the API is working

# Contributing to knockout-api

1. Make a feature branch targeting qa
2. Do work, push it to your branch
3. Write tests
4. Create a pull request

# Development procedure


// config/config.ts
require('dotenv').config();

const config = {
    "production": {
        "username": process.env.DB_USERNAME,
        "password": process.env.DB_PASSWORD,
        "database": process.env.DB_NAME,
        "host": process.env.DB_HOST,
        "certDomain": process.env.CERT_DOMAIN,
        "dialect": "mysql",
        "dialectOptions": {
          "charset": 'utf8mb4'
        },
    },
    "qa": {
        "username": process.env.DB_USERNAME_QA,
        "password": process.env.DB_PASSWORD_QA,
        "database": process.env.DB_NAME_QA,
        "host": process.env.DB_HOST_QA,
        "certDomain": process.env.CERT_DOMAIN_QA,
        "dialect": "mysql",
        "dialectOptions": {
          "charset": 'utf8mb4'
        },
    },
    "development": {
        "username": process.env.DB_USERNAME_DEV,
        "password": process.env.DB_PASSWORD_DEV,
        "database": process.env.DB_NAME_DEV,
        "host": process.env.DB_HOST_DEV,
        "certDomain": process.env.CERT_DOMAIN,
        "dialect": "mysql",
        "dialectOptions": {
          "charset": 'utf8mb4'
        },
        "logging": true,
        // Need to set max-allowed-packet here so we can
        // bulk insert a lot of data for seeding
        "max-allowed-packet": "200M",
    },
    "test": {
        "username": process.env.DB_USERNAME_TEST,
        "password": process.env.DB_PASSWORD_TEST,
        "database": process.env.DB_NAME_TEST,
        "host": process.env.DB_HOST_TEST,
        "certDomain": process.env.CERT_DOMAIN_TEST,
        "dialect": "mysql",
        "dialectOptions": {
          "charset": 'utf8mb4'
        },
    }
};

// For ES6/TypeScript
export default config;

// For Sequelize
module.exports = config;

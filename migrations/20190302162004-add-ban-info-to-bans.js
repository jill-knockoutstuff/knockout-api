'use strict';

const knex = require('../src/services/knex').knex;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await knex.schema.table('Bans', function(table) {
      table.integer('banned_by');
      table.foreign('banned_by').references('Users.id');
    });

    await knex.schema.table('Bans', function(table) {
      table.integer('post_id').unsigned();
      table.foreign('post_id').references('Posts.id');
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Bans', 'post_id');
    return queryInterface.removeColumn('Bans', 'banned_by');
  }
};

// @ts-check
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Add column
    await queryInterface.addColumn('Threads', 'deleted_at', {
      allowNull: true,
      type: Sequelize.DATE
    });
    await queryInterface.addColumn('Threads', 'locked', {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: false
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Threads', 'deleted_at');
    return queryInterface.removeColumn('Threads', 'locked');
  }
};

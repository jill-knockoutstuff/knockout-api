'use strict';

const knex = require('../src/services/knex').knex;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await knex.raw('ALTER TABLE Posts MODIFY COLUMN content LONGTEXT DEFAULT NULL NULL');
  },
  
  down: async (queryInterface, Sequelize) => {
    await knex.raw('ALTER TABLE Posts MODIFY COLUMN content JSON DEFAULT NULL NULL');
  }
};

// @ts-check
'use strict';

// READ ME
// This creates an index on one of the biggest tables we have.
// With 3 million Post records, it takes about 25 seconds to complete.
// Please make sure that this migration is ran during a non-busy period
// to avoid any possible slowness with the Posts table,
// but Users should still be able to make posts as the index is being created.
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Posts', ['created_at'])
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Posts', 'posts_created_at');
  }
};

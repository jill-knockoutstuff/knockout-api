'use strict';

import { internet, date } from 'faker';

const USER_COUNT = 10000;

const randomUserArray = () => {
  return Array.from({ length: USER_COUNT }).map(() => {
    return {
      username: internet.userName(),
      email: internet.email(),
      avatar_url: '',
      background_url: '',
      usergroup: 1,
      external_type: 'Google',
      external_id: 'GoogleID',
      created_at: date.past(3),
      updated_at: date.past(2),
    };
  });
}

export function up(queryInterface, Sequelize) {
  return queryInterface.bulkInsert('Users', randomUserArray());
}
export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Users', null, {});
}

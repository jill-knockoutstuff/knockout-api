'use strict';

import { random, date } from 'faker';

const THREAD_PER_SUBFORUM_COUNT_MAX = 2000;
const ICON_ID_COUNT = 30;

const randomInt = (max) => {
  return Math.floor(Math.random() * (max - 1) + 1);
}

const randomThread = (userId, subforumId) => {
  return {
    title: random.words(5),
    icon_id: randomInt(ICON_ID_COUNT),
    user_id: userId,
    subforum_id: subforumId,
    created_at: date.past(2),
    updated_at: date.past(1),
    locked: false,
    pinned: false,
    background_url: '',
    background_type: '',
  }
}

const randomThreads = (subforumIds, userIds) => {
  const threads = subforumIds.map((subforumId) => {
    const threadCount = randomInt(THREAD_PER_SUBFORUM_COUNT_MAX);

    return Array.from({ length: threadCount }).map(() => {
      const userId = userIds[randomInt(userIds.length)];
      return randomThread(userId, subforumId);
    })
  })

  return [].concat(...threads);
}

export async function up(queryInterface, Sequelize) {
  const subforumIds = await queryInterface.sequelize.query(
    `SELECT id FROM Subforums`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  const userIds = await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  return queryInterface.bulkInsert('Threads', randomThreads(subforumIds, userIds), {});
}

export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Threads', null, {});
}

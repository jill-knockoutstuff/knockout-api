'use strict';

import { date } from 'faker';

const POST_RATING_COUNT_MAX = 40;
const RATINGS_COUNT = 14;

const randomInt = (max) => {
  return Math.floor(Math.random() * (max - 1) + 1);
}

const randomRating = (postId, userId, ratingId) => {
  return {
    post_id: postId,
    user_id: userId,
    rating_id: ratingId,
    created_at: date.past(1),
    updated_at: date.past(1),
  }
}

const insertRatings = async (queryInterface, postIds, userIds) => {
  // create ratings in random posts by random users
  let i, j, temparr, chunkSize = 1000;
  for (i = 0, j = postIds.length; i < j; i+=chunkSize) {
    temparr = postIds.slice(i, i+chunkSize);

    const ratings = [];
    temparr.forEach((postId) => {
      // 1/4 chance to not rate at all
      let ratingsCount;
      if (Math.random() < 0.25) {
        return;
      } else {
        ratingsCount = randomInt(POST_RATING_COUNT_MAX);
      }

      let userIdPool = userIds.slice(0);
      const ratingId = randomInt(RATINGS_COUNT);
      ratings.push(
        Array.from({ length: ratingsCount }).map(() => {
          const randomIdIndex = randomInt(userIdPool.length);
          const userId = userIdPool[randomIdIndex];
          userIdPool.splice(randomIdIndex, 1);
          return randomRating(postId, userId, ratingId);
        })
      )
    })
    await queryInterface.bulkInsert('Ratings', [].concat(...ratings));
  }
  return true;
}

export async function up(queryInterface, Sequelize) {
  const postIds = await queryInterface.sequelize.query(
    'SELECT id FROM Posts LEFT JOIN Ratings ON Ratings.post_id = Posts.id',
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  const userIds = await queryInterface.sequelize.query(
    `SELECT id FROM Users`,
    { type: Sequelize.QueryTypes.SELECT }
  ).map((record) => record.id);

  return insertRatings(queryInterface, postIds, userIds);
}

export function down(queryInterface, Sequelize) {
  return queryInterface.bulkDelete('Ratings', null, {});
}

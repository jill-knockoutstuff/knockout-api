import { OAuth2Client } from 'google-auth-library';
import { loginWithExternalId } from './common';
import { createRedirectUrl } from '../../helpers/utils';

import { NODE_ENV, GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET } from '../../../config/server';
import { Request, Response } from 'express';

const clientId = GOOGLE_CLIENT_ID;
const clientSecret = GOOGLE_CLIENT_SECRET;

const loginRedirect = (req: Request) => {
  const proto = (NODE_ENV === "development") ? 'http' : 'https';
  return createRedirectUrl(req, '/auth/google/callback', proto);
};

export default {

  login(req: Request, res: Response) {
    const redirect = loginRedirect(req);
    const client = new OAuth2Client(clientId, clientSecret, redirect);
    res.redirect(client.generateAuthUrl({ scope: 'profile', prompt: 'select_account' }));
  },

  async callback(req: Request, res: Response) {
    try {
      const { code } = req.query;

      const client = new OAuth2Client(clientId, clientSecret, loginRedirect(req));

      // Get the access token using the authorization code from the OAuth redirect
      const { tokens } = await client.getToken(code);

      // Get the Google user ID
      const { sub: googleId } = await client.getTokenInfo(tokens.access_token);

      // Log the login attempt
      console.log(`Google user logged in: ${googleId}`);

      // Finish by logging in the user
      await loginWithExternalId('GOOGLE', googleId, res);

    } catch(err) {
      console.error(err ? err.stack : err);
      res.sendStatus(500);
    }
  }

}

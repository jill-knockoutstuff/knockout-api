import passport, { Profile } from 'passport';
import { loginWithExternalId } from './common';
import * as config from '../../../config/server';
import { Request } from 'express';

/**
 * @param isOpenID Whether the provider is OpenID-based (like Steam) and thus
 * has a different callback format (using req, id, profile, done as parameters)
 */
function createLoginCallback(provider: string, isOpenID?: boolean) {
  const loginCallback = function (req: Request, accessToken: string, refreshToken: string, profile: Profile, done: () => void) {
    const res = req.res;
    if (accessToken) {
      loginWithExternalId(provider, profile.id, res);
    } else {
      res.status(401).json({ error: 'Unauthorized', message: 'Authentication provider did not provide an access token' });
    }
    // Do not call "done" because it will make Passport handle the user token, but we want to validate it manually
  }
  if (isOpenID) {
    // Skip the refreshToken when calling the callback
    return (req: Request, id: string, profile: passport.Profile, done: () => void) => loginCallback(req, id, null, profile, done);
  } else {
    return loginCallback;
  }
}

// Twitter
if (config.TWITTER_CONSUMER_KEY) {
  const Twitter = require('passport-twitter');
  const callback = createLoginCallback('TWITTER');
  passport.use(new Twitter.Strategy({
      userAuthorizationURL: 'https://api.twitter.com/oauth/authenticate?force_login=true',
      consumerKey: config.TWITTER_CONSUMER_KEY,
      consumerSecret: config.TWITTER_CONSUMER_SECRET,
      callbackURL: '/auth/twitter/callback',
      passReqToCallback: true
    },
    callback
  ));
}

// GitHub
if (config.GITHUB_CLIENT_ID) {
  const GitHub = require('passport-github');
  const callback = createLoginCallback('GITHUB');
  passport.use(new GitHub.Strategy({
      authorizationURL: 'https://github.com/login/oauth/authorize?login=',
      clientID: config.GITHUB_CLIENT_ID,
      clientSecret: config.GITHUB_CLIENT_SECRET,
      callbackURL: '/auth/github/callback',
      passReqToCallback: true
    },
    callback
  ));
}

// Steam
if (config.STEAM_REALM) {
  const Steam = require('passport-steam');
  const callback = createLoginCallback('STEAM', true);
  passport.use(new Steam.Strategy({
      realm: config.STEAM_REALM,
      apiKey: config.STEAM_API_KEY,
      returnURL: `${config.STEAM_REALM}/auth/steam/callback`,
      passReqToCallback: true
    },
    callback
  ));
}

export default {
  twitter: passport.authenticate('twitter'),
  github: passport.authenticate('github'),
  steam: passport.authenticate('steam', { prompt: 'consent' }),
  patreon: passport.authenticate('patreon'),
}

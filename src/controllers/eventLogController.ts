import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';

interface ReqUser {
  user: {
    id: number
  }
}

export const store = async (req: ReqUser, content: any) => {
  try {
    const result = knex('Events')
      .insert({
        executed_by: req.user.id,
        content: JSON.stringify({ content })
      })
      .then(rows => rows)
      .catch(err => console.log(err));

    return result;
  } catch (exception) {
    console.log(exception);

    return { error: exception };
  }
};

export const index = async (req: Request, res: Response) => {
  try {
    let events = await knex
      .select('*')
      .from('Events')
      .orderBy('created_at', 'DESC')
      .limit(40);

    events = Array.isArray(events) ? events : [events];

    res.status(httpStatus.OK);
    res.json(events);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const threadStatusEvent = async ({ userId, threadId, type, username }) => {
  let updateType = null;
  let emoji = null;
  if (type.locked === true) {
    emoji = '🔒';
    updateType = 'locked';
  } else if (type.locked === false) {
    emoji = '🔓';
    updateType = 'unlocked';
  } else if (type.deleted_at) {
    emoji = '🗑️';
    updateType = 'deleted';
  } else if (type.deleted_at === null) {
    emoji = '🗑️';
    updateType = 'restored';
  } else if (type.pinned) {
    emoji = '📌';
    updateType = 'pinned';
  } else if (type.unpinned === false) {
    emoji = '📌';
    updateType = 'unpinned';
  }

  if (!updateType) {
    return { error: 'Unknown thread update event type' };
  }

  const thread = await knex
    .select('title', 'id')
    .first()
    .from('Threads')
    .whereRaw('id = ?', [threadId]);

  const req = { user: { id: userId } };
  const content = [
    `${emoji} ${username} ${updateType} `,
    {
      text: thread.title,
      link: `/thread/${thread.id}`
    },
    '.'
  ];

  store(req, content);
};

export const banEvent = async ({ userId, expiresAt, banReason, postId, bannedBy }) => {
  const [{ username: bannedUsername }, { username: moderatorUsername }] = await knex
    .select('username')
    .from('Users')
    .whereIn('id', [userId, bannedBy])
    .orderByRaw(knex.raw(`FIELD(id,${bannedBy})`));

  const post = await knex
    .select('thread_id')
    .first()
    .from('Posts')
    .whereRaw('id = ?', [postId]);

  const content = [
    `🔨 ${moderatorUsername} banned `,
    `${bannedUsername} in `,
    {
      text: 'this thread',
      link: `/thread/${post.thread_id}`
    },
    ` with reason "${banReason}" until ${expiresAt}.`
  ];

  const req = { user: { id: bannedBy } };

  store(req, content);
};

export const threadUpdateEvent = async ({ username, oldTitle, newTitle, threadId, userId, body }) => {
  let content = null;

  if (body.title) {
    content = [
      `📝 ${username} renamed `,
      `${oldTitle} to `,
      {
        text: newTitle,
        link: `/thread/${threadId}`
      },
      '.'
    ];
  } else if (body.subforum_id) {
    const subforum = await knex
      .select('name', 'id')
      .first()
      .from('Subforums')
      .whereRaw('id = ?', [body.subforum_id]);

    content = [
      `↪️ ${username} moved `,
      {
        text: newTitle,
        link: `/thread/${threadId}`
      },
      ` to `,
      {
        text: subforum.name,
        link: `/subforum/${subforum.id}`
      }
    ];
  }

  const req = { user: { id: userId } };

  store(req, content);
};

export const removeUserAvatarBg = async ({ userId, avatar, background, removedBy }) => {
  const response = await knex
    .select('username')
    .from('Users')
    .whereIn('id', [userId, removedBy])
    .orderByRaw(knex.raw(`FIELD(id,${removedBy})`));

  const targetUsername = response[0].username;
  const moderatorUsername = response[1] ? response[1].username : response[0].username;

  const avatarString = avatar ? 'avatar' : '';
  const backgroundString = background ? 'background' : '';
  const bothString = avatar && background ? ' and ' : '';

  const content = [
    `🔞 ${moderatorUsername} removed `,
    `${targetUsername}'s `,
    `${avatarString}${bothString}${backgroundString} `,
    `because of their poor taste.`
  ];

  const req = { user: { id: removedBy } };

  store(req, content);
};

export const unbanUser = async ({ userId, removedBy }) => {
  const [{ username: targetUsername }, { username: moderatorUsername }] = await knex
    .select('username')
    .from('Users')
    .whereIn('id', [userId, removedBy])
    .orderByRaw(knex.raw(`FIELD(id,${removedBy})`));

  const content = [`🙏 ${moderatorUsername} invalidated one of `, `${targetUsername}'s `, `bans.`];

  const req = { user: { id: removedBy } };

  store(req, content);
};

export const becomeGoldEvent = async ({ username, userId }) => {
  const content = [
    `👑 ${username} has ascended to Goldhood!`
  ];

  const req = { user: { id: userId } };

  store(req, content);
};

export const loseGoldEvent = async ({ username, userId }) => {
  const content = [
    `🤢 ${username}'s Gold Membership has run out... 😞`
  ];

  const req = { user: { id: userId } };

  store(req, content);
};

export const threadBackgroundUpdateEvent = async ({ userId, username, threadId, threadTitle }) => {
  const content = [
    `🖼️ ${username} changed the background of `,
    {
        text: threadTitle,
        link: `/thread/${threadId}`
    }
  ];

  const req = { user: { id: userId } };

  store(req, content);
};

export const threadAutoLock = async ({ threadId }) => {
  try {
    const thread = await knex
    .select('title')
    .first()
    .from('Threads')
    .whereRaw('id = ?', [threadId]);

    const content = [
      `🍃 `,
      {
        text: thread.title,
        link: `/thread/${threadId}`
      },
      ` has reached 1000 posts and has been autolocked.`,
      ` Now it's gone - like leaves in the wind.`
    ];

    const req = { user: { id: 1 } };

    store(req, content);
  } catch (error) {
    console.log(error)
  }
};

export const removeUserProfile = async ({ userId, removedBy }) => {
  const response = await knex
    .select('username')
    .from('Users')
    .whereIn('id', [userId, removedBy])
    .orderByRaw(knex.raw(`FIELD(id,${removedBy})`));

  const targetUsername = response[0].username;
  const moderatorUsername = response[1] ? response[1].username : response[0].username;

  const content = [
    `🔞 ${moderatorUsername} removed `,
    `${targetUsername}'s `,
    `user profile customizations because of their poor taste.`
  ];

  const req = { user: { id: removedBy } };

  store(req, content);
};


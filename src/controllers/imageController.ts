import { Request, Response } from "express";

import httpStatus from 'http-status';
import sharp from 'sharp';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from "../../config/server";
import { isCachedUserGoldMembershipActive } from "./productController";

// all of this should be extracted into a microservice

export const show = async (req: Request, res: Response) => {
  try {
    res.status(httpStatus.OK);
    res.setHeader("Cache-Control", "public, max-age=120");
    res.setHeader("Expires", new Date(Date.now() + 120000).toUTCString());
    res.sendFile(global.__basedir + '/static/avatars/' + req.params.filename);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const avatarUpload = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }
    if (!req.file || !req.file.mimetype || !req.file.mimetype.startsWith('image/')) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'Unprocessable entity.' });
      return;
    }

    let fileName = req.user.id + '.webp';
    let filePath = (NODE_ENV === "production") ? 'image/' + fileName : 'avatars/' + fileName;

    const { format, width, height, size, pages } = await sharp(req.file.buffer).metadata();

    // validate file matching all requirements for animated avatars
    if ( format === 'gif' && pages > 1 && width <= 115 && height <= 115 && size < 125000 ) {
      // only allow animated avatar if user has an active, CACHED gold membership
      const isActive = await isCachedUserGoldMembershipActive(req.user);

      if (isActive) {
        // run sharp with no resizing - important to run the validations first
        const encodedImageBuffer = await sharp(req.file.buffer, { pages: -1 })
          .withMetadata()
          .toFormat('webp')
          .toBuffer();

        await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

        res.status(httpStatus.CREATED);
        res.json({ message: `${req.user.id}.webp` });
        return;
      }
    }

    // regular sharp conversion. no animation support
    let encodedImageBuffer = await sharp(req.file.buffer)
      .withMetadata()
      .resize(115, 115, {
        withoutEnlargement: true,
        fit: 'inside'
      })
      .toFormat('webp')
      .toBuffer();

    await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

    res.status(httpStatus.CREATED);
    res.json({ message: `${req.user.id}.webp` });
  } catch (exception) {
    console.log(exception);
    console.log('Errored. Ending. ===');

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const backgroundUpload = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }
    if (!req.file || !req.file.mimetype || !req.file.mimetype.startsWith('image/')) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({ error: 'Unprocessable entity.' });
      return;
    }

    let fileName = req.user.id + '-bg.webp';
    let filePath = (NODE_ENV === "production") ? 'image/' + fileName : 'avatars/' + fileName;
    let encodedImageBuffer = await sharp(req.file.buffer)
      .resize(230, 460)
      .webp({ quality: 75, nearLossless: true, lossless: true })
      .toFormat('webp')
      .toBuffer();

    await fileStore.storeBuffer(encodedImageBuffer, filePath, 'image/webp');

    res.status(httpStatus.CREATED);
    res.json({ message: `${req.user.id}-bg.webp` });
  } catch (exception) {
    console.log(exception);
    console.log('Errored. Ending. ===');

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

import { Request, Response } from "express";

import httpStatus from 'http-status';
import knex from '../services/knex';

export const createMention = async ({
  isLoggedIn,
  postId,
  mentionsUser,
  content,
  inactive,
  threadId,
  page,
  userId
}) => {
  try {
    if (!isLoggedIn) {
      throw new Error('Bad user login on mentionsController.store');
    }

    const result = await knex('Mentions').insert({
      post_id: postId,
      mentions_user: mentionsUser,
      content: content,
      inactive: inactive,
      thread_id: threadId,
      page,
      mentioned_by: userId
    });

    return result;
  } catch (exception) {
    console.log(exception);

    return new Error(exception);
  }
};

export const markAsRead = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on mentionsController.markAsRead');
    }
    if (!req.body.postIds) {
      throw new Error('No postIds supplied.');
    }

    const postIds = Array.isArray(req.body.postIds) ? req.body.postIds : [req.body.postIds];

    const result = await knex('Mentions')
      .update({ inactive: true })
      .where({ mentions_user: req.user.id })
      .whereIn('post_id', postIds)

    res.status(httpStatus.OK);
    res.json({ message: 'Posts marked as read!' });
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const getMentionsQuery = async (userId: number) => {
  const results = await knex('Mentions')
    .select(
      'Mentions.id as mentionId',
      'Mentions.post_id as postId',
      'Mentions.content',
      'Mentions.created_at as createdAt',
      'Mentions.thread_id as threadId',
      'Mentions.page as threadPage',
    )
    .where({ mentions_user: userId, inactive: false })
    .orWhere({ mentions_user: userId, inactive: null })
    .limit(20);

  return Array.isArray(results) ? results : [results];
}

export const index = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json([]);
      return;
    }

    const results = await getMentionsQuery(req.user.id);

    res.status(httpStatus.OK);
    res.json(results);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

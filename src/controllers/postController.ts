import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import redis from '../services/redisClient';

const datasource = require('../models/datasource');
const { invalidateObject, getFormattedObject } = require('../retriever/thread');
const eventLogController = require('./eventLogController');

import composer from '../helpers/queryComposer';
import responseStrategy from '../helpers/responseStrategy';
import Pagination from '../helpers/PaginationBuilder';
import { isProxy } from '../helpers/proxyDetect';
import postMentionFinder, { bbMentionFinder } from '../helpers/postMentionFinder';
import { getCountryName } from '../helpers/geoipLookup';
import { validateAppName } from '../validations/post';

const { validateThreadStatus, validatePostLength } = require('../validations/post');
const { validateUserFields, validateUserAge } = require('../validations/user');

const { createMention } = require('./mentionsController');

const { Post, Rating } = datasource().models;

const MIN_ACCT_AGE_SUBFORUM_IDS = [5, 6]; // subforumIds where acct has to be at least 3 days old to post

export const index = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  options.distinct = true;
  options.col = 'Post.id';

  try {
    const result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    responseStrategy.send(res, pagResult, options);
  } catch (error) {
    console.error(error);
    responseStrategy.send(res, error, options);
  }
};
exports.show = (req, res) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  scope
    .findOne(options)
    .then(result => {
      redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

      responseStrategy.send(res, result, options);
    })
    .catch(err => {
      console.error(err);
      responseStrategy.send(res, err, options);
    });
};
export const store = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on postController.store');
    }
    if (req.user.isBanned) {
      throw new Error('User is banned');
    }

    // validations for a valid post
    if (
      !(await validateThreadStatus(req.body.thread_id)) ||
      !validatePostLength(req.body.content)
    ) {
      throw new Error('Failed validations.');
    }
    if (!validateUserFields(req.user)) {
      throw new Error('Invalid user.');
    }
    // client name logic
    let appName: string | null;

    if (req.body.appName) {
      appName = validateAppName(req.body.appName)
        ? req.body.appName
        : null;
    } else {
      appName = null;
    }

    // is the user under a week old?
    const isNew = !(await validateUserAge(req.user));
    if (isNew) {
      // are they using a VPN or do they look like they're in a datacenter?
      const ip = req.ipInfo;
      if (isProxy(ip)) {
        console.log('VPN detected', ip);
        throw new Error('You appear to be using a proxy/VPN.');
      }

      // is this user trying to post in News/Politics?
      const thread = await getFormattedObject(req.body.thread_id);
      if (MIN_ACCT_AGE_SUBFORUM_IDS.includes(thread.subforumId)) {
        res.status(httpStatus.UNPROCESSABLE_ENTITY);
        throw new Error(`Your account (${req.user.id} - ${req.user.username}) is too new to post in this subforum. (ERR2NU: LURKMOAR)`);
      }
    }

    //
    // automerge logic
    // if the latest post in a thread is by this
    // user, merge posts instead of creating a new one
    //
    const lastPostQuery = await knex
      .from('Posts as po')
      .select('po.id', 'po.content', 'po.user_id')
      .where(
        knex.raw('po.created_at >= DATE_SUB(NOW(), INTERVAL 3 HOUR) AND po.thread_id = ?', [
          req.body.thread_id
        ])
      )
      .limit(1)
      .orderByRaw('po.id DESC');

    let post = null;
    let lastPostId = null;
    if (lastPostQuery.length > 0 && lastPostQuery[0].user_id === req.user.id) {
      // automerge
      lastPostId = lastPostQuery[0].id;
      let postContent;
      let newContent;
      let resultingContent;

      try {
        postContent = JSON.parse(lastPostQuery[0].content);
        newContent = JSON.parse(req.body.content);

        let edit = [
          {
            object: 'block',
            type: 'paragraph',
            data: {},
            nodes: [
              {
                object: 'text',
                leaves: [
                  {
                    object: 'leaf',
                    text: '',
                    marks: []
                  }
                ]
              }
            ]
          },
          {
            object: 'block',
            type: 'paragraph',
            data: {},
            nodes: [
              {
                object: 'text',
                leaves: [
                  {
                    object: 'leaf',
                    text: 'Edit:',
                    marks: [
                      {
                        object: 'mark',
                        type: 'bold',
                        data: {}
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            object: 'block',
            type: 'paragraph',
            data: {},
            nodes: [
              {
                object: 'text',
                leaves: [
                  {
                    object: 'leaf',
                    text: '',
                    marks: []
                  }
                ]
              }
            ]
          }
        ];

        postContent.document.nodes = postContent.document.nodes.concat(
          edit,
          newContent.document.nodes
        );
        resultingContent = JSON.stringify(postContent);
      } catch (error) {
        postContent = lastPostQuery[0].content;
        newContent = req.body.content;

        resultingContent = `${postContent}
[b]Edited:[/b]
${newContent}`;
      }

      await knex('Posts')
        .where({ id: lastPostId })
        .update({
          updated_at: new Date(),
          content: resultingContent,
          app_name: appName
        });

      post = await knex('Posts')
        .select('id', 'user_id', 'thread_id')
        .where({ id: lastPostId });
      post = post[0];
    } else {
      // country information logic
      const countryName = req.body.displayCountryInfo ? getCountryName(req.ipInfo) : null;

      // new post
      post = await Post.create({
        ...req.body,
        content: typeof req.body.content === 'string' ? req.body.content : JSON.stringify(req.body.content),
        user_id: req.user.id,
        ip_address: req.ipInfo,
        country_name: countryName,
        app_name: appName,
      });
      lastPostId = post.id;
    }

    const scope = composer.scope(req, Post);
    const result = await scope.findOne({ where: { id: lastPostId } });

    //
    // if a thread has 1000 posts or more,
    // it should be locked automatically.
    //
    const threadPostcount = await knex('Posts')
      .where({ thread_id: req.body.thread_id })
      .count('id');
    const lockedStatus = parseInt(threadPostcount[0]['count(`id`)'], 10) >= 1000;

    if (lockedStatus) {
      eventLogController.threadAutoLock({
        threadId: req.body.thread_id
      });
    }

    //
    // update Thread updated_at
    //
    await knex('Threads')
      .where({ id: req.body.thread_id })
      .update('updated_at', new Date())
      .update('locked', lockedStatus ? 1 : undefined);

    //
    // invalidate thread object cache
    //
    invalidateObject(req.body.thread_id);

    //
    // mentions logic
    //
    if (post && post && post.id) {
      // go through the entire value again and look for mentions :/
      const mentions = bbMentionFinder(req.body.content);

      if (mentions && mentions.length > 0) {
        const thread = await knex('Threads')
          .select('title')
          .where('id', req.body.thread_id);

        // build message notification
        const content = JSON.stringify([
          `💬 ${req.user.username} mentioned you in "${thread[0].title}".`
        ]);

        // construct the page number
        const postCount = threadPostcount[0]['count(`id`)'];
        const page = Math.ceil(parseInt(postCount, 10) / 20);

        for (let i = 0; i < mentions.length; i++) {
          const userId = mentions[i];

          // let's not mention ourselves, that'd be kinda desperate
          if (userId !== req.user.id) {
            await createMention({
              isLoggedIn: req.isLoggedIn,
              postId: post.id,
              mentionsUser: userId,
              content,
              inactive: false,
              threadId: req.body.thread_id,
              page,
              userId: req.user.id
            });
          }
        }
      }
    }

    if (req.returnEarly === true) {
      return;
    } else {
      res.status(httpStatus.CREATED);
      res.json(result);
    }
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};
export const update = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on postController.store');
    }

    // validations for a valid post
    if (!validateThreadStatus(req.body.thread_id) || !validatePostLength(req.body.content)) {
      throw new Error('Failed validations.');
    }
    if (!validateUserFields(req.user)) {
      throw new Error('Invalid user.');
    }
    if (req.user.isBanned) {
      throw new Error('User is banned.');
    }

    const userIsMod = [3, 4, 5].includes(req.user.usergroup);

    const targetUser = userIsMod ? {} : { user_id: req.user.id };

    const contentToDb = typeof req.body.content === 'string' ? req.body.content : JSON.stringify(req.body.content);

    // ip address:
    // if you're a mod and you're not editing your own post,
    // use the existing IP on the post.
    let ipAddress = req.ipInfo;
    if (userIsMod) {
      const existingPost = await knex('Posts')
        .select('ip_address', 'user_id')
        .where({ id: req.body.id, ...targetUser });

      if (existingPost.user_id !== req.user.id) {
        ipAddress = existingPost.ip_address;
      }
    }
    // client name logic
    let appName: string | null;

    if (req.body.appName) {
      appName = validateAppName(req.body.appName)
        ? req.body.appName
        : null;
    } else {
      appName = null;
    }

    const result = await knex('Posts')
      .where({ id: req.body.id, ...targetUser })
      .update({ content: contentToDb, updated_at: knex.fn.now(), ip_address: ipAddress, app_name: appName });

    res.status(httpStatus.CREATED);
    res.json({ message: 'Post updated' });
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};
export const destroy = async (req: Request, res: Response) => {
  Post.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};
exports.count = (req, res) => {
  const options = composer.onlyQuery(req);

  Post.count(options)
    .then(result => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch(err => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};
export const withPostsAndCount = async (req: Request, res: Response) => {
  // if no page parameter is provided, default to page 1
  const pageParam = req.params.page || 1;

  try {
    const query = await knex
      .from('Posts as po')
      .select(
        'po.id as postId',
        'po.content as postContent',
        'po.user_id as postUserId',
        'po.created_at as postCreatedAt',
        'po.updated_at as postUpdatedAt',
        'po.thread_id as postThreadId',
        'us.username as userUsername',
        'us.usergroup as userUsergroup',
        'us.avatar_url as userAvatar_url',
        'us.background_url as userBackgroundUrl',
        'us.created_at as userCreated_at',
        knex.raw(
          '(select count (*) from Bans where Bans.user_id = po.user_id AND expires_at > NOW()) as postUserBans'
        )
      )
      .leftJoin('Ratings as rat', 'po.id', 'rat.post_id')
      .join('Users as us', 'us.id', 'po.user_id')
      .where(knex.raw(`po.id = ?`, req.params.id));

    const queryResults = Array.isArray(query) ? query : [query];

    if (queryResults[0].postId == null) {
      return responseStrategy.send(res, null);
    }

    const ratings = await Rating.getRatingsForPostsWithUsers([queryResults[0].postId]);

    const response = {
      id: queryResults[0].postId,
      content: queryResults[0].postContent,
      createdAt: queryResults[0].postCreatedAt,
      updatedAt: queryResults[0].postUpdatedAt,
      user: {
        id: queryResults[0].postUserId,
        username: queryResults[0].userUsername,
        usergroup: queryResults[0].userUsergroup,
        avatar_url: queryResults[0].userAvatar_url,
        backgroundUrl: queryResults[0].userBackgroundUrl,
        createdAt: queryResults[0].userCreated_at,
        isBanned: queryResults[0].postUserBans > 0
      },
      ratings: ratings[queryResults[0].postId]
    };

    return responseStrategy.send(res, response);
  } catch (error) {
    console.error(error);
    responseStrategy.send(res, error);
  }
};

import knex from '../services/knex';
import redis from '../services/redisClient';

// returns a date for the first day of next month
function nextMonthExpiryDate() {
  const nextMonth = (new Date().getMonth() + 1) % 12 + 1;
  const nextYear = nextMonth === 1 ? new Date().getFullYear() + 1 : new Date().getFullYear();

  return new Date(`${nextMonth}-01-${nextYear}`);
}

export async function createGoldProductSubscriptionsForUser(user: User, productIds: number[], callbackFn: Function, expiresAt?: Date) {
  if (user.usergroup === 2) {
    return callbackFn({ error: 'User cannot receive perks.' });
  }

  try {
    const expiryDate = expiresAt ? expiresAt : new Date

    const rowsToInsert = productIds.map(productId => ({
      user_id: user.id,
      product_id: productId,
      expires_at: expiresAt || nextMonthExpiryDate()
    }));

    await knex('ProductSubscriptions').insert(rowsToInsert);

    return callbackFn({ message: 'User has been assigned perks. Should be active in ~5 mins.' });
  } catch (error) {
    console.error(error);

    return callbackFn({ error: 'User cannot receive perks.' });
  }
}

export async function isCachedUserGoldMembershipActive(user: User) {
  const cached = await redis.getAsync(`product-gold-membership-user-${user.id}`);
  return JSON.parse(cached);
}
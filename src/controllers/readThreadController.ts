import { Request, Response } from 'express';
import httpStatus from 'http-status';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import knex from '../services/knex';

export const store = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on readThreadController.store');
    }

    const result = onDuplicateUpdate(knex, 'ReadThreads', {
      user_id: req.user.id,
      thread_id: req.body.threadId,
      last_seen: new Date(req.body.lastSeen) || new Date()
    });

    res.status(httpStatus.CREATED);
    res.send('read thread #' + req.body.threadId );
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on readThreadController.destroy');
    }

    await knex('ReadThreads')
      .where({ user_id: req.user.id, thread_id: req.body.threadId })
      .del();

    res.status(httpStatus.OK);
    res.json({ message: 'Deleted readThread for thread #' + req.body.threadId });
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const index = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.OK);
      res.json([]);
      return;
    }

    const userIsMod = [3, 4, 5].includes(req.user.usergroup);

    let results = await knex('readThread as al')
      .select('*')
      .whereRaw(`al.user_id = ? ${userIsMod ? '' : 'AND th.locked = 0 AND th.deleted_at IS NULL'}`, [req.user.id])
      .leftJoin('Threads as th', 'th.id', 'al.thread_id')
      .select(
        'th.id as threadId',
        'th.title as threadTitle',
        'th.user_id as threadUser',
        'th.icon_id as threadIcon',
        'th.locked as threadLocked',
        'th.created_at as threadCreatedAt',
        'th.updated_at as threadUpdatedAt',
        'th.deleted_at as threadDeletedAt',
        'u.username as threadUsername',
        'u.avatar_url as threadUserAvatarUrl',
        'u.usergroup as threadUserUsergroup',
        knex.raw('(select count (*) from Posts where Posts.thread_id = th.id) as threadPostCount'),
        knex.raw(
          '(select count (*) from Posts where Posts.thread_id = th.id and Posts.created_at > al.last_seen) as unreadPosts'
        ),
        knex.raw(
          '(select id from Posts where Posts.thread_id = th.id and Posts.created_at > al.last_seen order by Posts.created_at limit 1 ) as firstUnreadId'
        ),
        knex.raw(
          '(select p.id from Posts p where p.thread_id = th.id order by p.created_at desc limit 1) as lastPostId'
        )
      )
      .leftJoin('Users as u', 'th.user_id', 'u.id')
      .orderBy('unreadPosts', 'desc')
      .limit(20);

    results = Array.isArray(results) ? results : [results];

    let threadMap = results.reduce(
      (map, th) => Object.assign(map, { [th.threadId]: th }),
      {}
    );

    let lastPosts = await knex
      .from('Posts as p')
      .select('p.id', 'p.created_at', 'p.thread_id', 'u.username', 'u.usergroup', 'u.avatar_url')
      .join('Users as u', 'p.user_id', 'u.id')
      .whereIn('p.id', results.map(th => th.lastPostId));

    lastPosts = Array.isArray(lastPosts) ? lastPosts : [lastPosts];

    // fill in the threads with their last posts
    Array.isArray(lastPosts)
      ? lastPosts.forEach(post => (threadMap[post.thread_id].lastPost = post))
      : [lastPosts].forEach(post => (threadMap[post.thread_id].lastPost = post));

    results.forEach(thread => thread.lastPost = thread.lastPost
      ? {
          id: thread.lastPost.id,
          created_at: thread.lastPost.created_at,
          thread: {
            id: thread.threadId,
            post_count: thread.threadPostCount,
            title: thread.threadTitle
          },
          user: {
            username: thread.lastPost.username,
            usergroup: thread.lastPost.usergroup,
            avatar_url: thread.lastPost.avatar_url
          }
        }
      : undefined)

    res.status(httpStatus.OK);
    res.json(Array.isArray(results) ? results : [results]);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

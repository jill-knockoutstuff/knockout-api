import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../../services/knex';
import DiscordWebhook from 'discord-webhook-ts';

const notify = async (reason: String) => {

  if (process.env.NODE_ENV !== 'production') return;

  const discordClient = new DiscordWebhook(
    process.env.MODERATION_WEBHOOK
  );

  await discordClient.execute({
    embeds: [
      {
        title: 'A new report has been received!',
        description: `Report description: "${reason}"
      Go get 'em, <@&551783790188691476>!
      https://knockout.chat/moderate/reports
      `,
        color: 16527151
      }
    ]
  });

};

export const post = async (req: Request, res: Response) => {

  if (!req.isLoggedIn) throw new Error('Bad user login on alertController.store');

  await knex('Reports').insert({
    post_id: req.body.postId,
    reported_by: req.user.id,
    report_reason: req.body.reportReason
  });

  await notify(req.body.reportReason);

  res.status(httpStatus.CREATED);
  res.json({ message: 'Created a report for post #' + req.body.postId });

};
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../../services/knex';
import moderatorOnly from '../../helpers/moderatorOnly';
import { reportsPerPage } from '../../constants/pagination';
import { getFormattedObjects } from '../../retriever/report';

const getResultCount = async () => {

  const result = await knex('Reports')
    .count({ count: 'id' })
    .first();

  return result.count;

};

const getResults = async (page: number) => {

  const results = await knex('Reports')
    .pluck('id')
    .orderBy('id', 'desc')
    .limit(reportsPerPage)
    .offset((page * reportsPerPage) - reportsPerPage)
    .debug(true);

  const output = await getFormattedObjects(results);

  return output;

};

export const get = async (req: Request, res: Response) => {

  moderatorOnly(req, res);

  const page = Number(req.params.page) || 1;
  const resultCount = await getResultCount();
  const results = await getResults(page);

  res.status(httpStatus.OK);
  res.json({
    totalReports: resultCount,
    currentPage: page,
    reports: results
  });

};
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../../services/knex';
import moderatorOnly from '../../helpers/moderatorOnly';

export const post = async (req: Request, res: Response) => {

  moderatorOnly(req, res);

  await knex('Reports')
    .where({ id: req.body.reportId })
    .update({ solved_by: req.user.id, updated_at: knex.fn.now() });

  res.status(httpStatus.CREATED);
  res.json({ message: 'Report marked as solved.' });

};
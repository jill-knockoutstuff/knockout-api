import { Request, Response } from 'express';
import ResponseStrategy from '../helpers/responseStrategy';
import knex from '../services/knex';
import redis from '../services/redisClient';

export const index = async (req: Request, res: Response) => {

  const cacheKey = 'stats';
  let stats: any = await redis.getAsync(cacheKey);
  if (typeof stats == 'string') {
    stats = JSON.parse(stats);
  } else {

    const query = await knex
      .select(
        knex.raw('(SELECT count(*) FROM Users) as userCount'),
        knex.raw('(SELECT count(*) FROM Posts) as postCount'),
        knex.raw('(SELECT count(*) FROM Threads) as threadCount'),
        knex.raw('(SELECT count(*) FROM Ratings) as ratingsCount')
      )
      .first();

    const {
      userCount,
      postCount,
      threadCount,
      ratingsCount
    } = query;

    stats = {
      userCount: userCount,
      postCount: postCount,
      threadCount: threadCount,
      ratingsCount: ratingsCount
    };

    redis.setex(cacheKey, 1800, JSON.stringify(stats));

  }

  return ResponseStrategy.send(res, stats);

};
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import composer from '../../helpers/queryComposer';
import ResponseStrategy from '../../helpers/responseStrategy';
import datasource from '../../models/datasource';
import knex from '../../services/knex';
import { getFormattedObjects as getFormattedThreadObjects } from '../../retriever/thread';
import { getFormattedObjects as getFormattedUserObjects } from '../../retriever/user';

const getSubforums = async () => {
  const subforumIdentifier = knex.raw('??', ['Subforums.id']);

  const totalThreads = knex('Threads')
    .count({all: '*'})
    .where('Threads.subforum_id', subforumIdentifier)
    .whereNull('Threads.deleted_at')
    .as('total_threads');

  const totalPosts = knex('Posts')
    .count({all: '*'})
    .where('Threads.subforum_id', subforumIdentifier)
    .whereNull('Threads.deleted_at')
    .leftJoin('Threads', 'Posts.thread_id', 'Threads.id')
    .as('total_posts');

  const lastPost = knex.raw(
    `(
      select
        p1.id
      from (
        select thread_id,
        max(id) as id
      from Posts
      group by thread_id
      ) as p1
      left join Threads
        on p1.thread_id = Threads.id
      where
        Threads.subforum_id = Subforums.id
        and Threads.deleted_at is null
      order by p1.id desc limit 1
    ) as last_post_id`
  )

  const subforums = await knex
    .select('*', totalThreads, totalPosts, lastPost)
    .orderBy('Subforums.created_at', 'asc')
    .from('Subforums');

  return subforums;
};

const getLastPosts = async (postIds) => {
  const lastPosts = await knex
    .select(
      'Posts.id as postId',
      'Posts.user_id as postUserId',
      'Posts.created_at as postCreatedAt',
      'Threads.subforum_id as subforumId',
      'Threads.id as threadId'
    )
    .leftJoin('Threads', 'Posts.thread_id', 'Threads.id')
    .whereIn('Posts.id', postIds)
    .from('Posts');

  const users = await getFormattedUserObjects(lastPosts.map((lastPost) => {
    return lastPost.postUserId;
  }));

  const threads = await getFormattedThreadObjects(lastPosts.map((lastPost) => {
    return lastPost.threadId;
  }));

  const detailedLastPosts = lastPosts.map((lastPost, index) => {
    return {
      id: lastPost.postId,
      created_at: lastPost.postCreatedAt, // deprecate
      createdAt: lastPost.postCreatedAt,
      user: users[index],
      thread: threads[index]
    };
  });

  return detailedLastPosts.reduce((list, lastPost) => {
    list[lastPost.thread.subforumId] = lastPost;
    return list;
  }, {});
};

export const getAll = async (req: Request, res: Response) => {
  const subforums = await getSubforums();
  const lastPostIds = subforums.map((subforum) => {
    return subforum.last_post_id;
  });
  const lastPosts = await getLastPosts(lastPostIds);

  const output = subforums.map((subforum) => {
    return {
      id: subforum.id,
      name: subforum.name,
      icon_id: subforum.icon_id, // deprecate
      iconId: subforum.icon_id,
      created_at: subforum.created_at, // deprecate
      createdAt: subforum.created_at,
      updated_at: subforum.updated_at, // deprecate
      updatedAt: subforum.updated_at,
      description: subforum.description,
      icon: subforum.icon,
      totalThreads: subforum.total_threads,
      totalPosts: subforum.total_posts,
      lastPost_id: subforum.last_post_id, // deprecate
      lastPostId: subforum.last_post_id,
      lastPost: lastPosts[subforum.id]
    }
  });

  ResponseStrategy.send(res, {
    list: output
  });
};

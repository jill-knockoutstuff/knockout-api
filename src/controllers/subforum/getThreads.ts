import { Request, Response } from 'express';
import httpStatus from 'http-status';
import composer from '../../helpers/queryComposer';
import ResponseStrategy from '../../helpers/responseStrategy';
import datasource from '../../models/datasource';
import knex from '../../services/knex';
import Thread from '../../retriever/thread';

const getSubforum = async (subforumId: number) => {
  const subforum = await knex
    .select('*')
    .from('Subforums')
    .where('id', subforumId)
    .first();
  return subforum;
};

const getThreadCount = async (subforumId:number, showDeleted: Boolean) => {
  const threadCount = await knex
    .count({all: '*'})
    .from('Threads')
    .where('subforum_id', subforumId)
    .where((builder) => {
      if (!showDeleted) {
        builder.whereNull('deleted_at');
      }
    })
    .first();
  return threadCount.all;
};

const getThreadIds = async (subforumId: number, offset: number, showDeleted: Boolean) => {
  let threadIds = await knex
    .select('Threads.id')
    .from('Threads')
    .where((builder) => {
      if (!showDeleted) {
        builder.whereNull('Threads.deleted_at');
      }
    })
    .where('Threads.subforum_id', subforumId)
    .orderBy('Threads.pinned', 'desc')
    .orderBy('Threads.updated_at', 'desc')
    .limit(40)
    .offset(offset);

  return threadIds.map((thread) => {
    return parseInt(thread.id);
  });
};

const showDeleted = async (req: Request) => {
  if (typeof req.user == 'undefined') return false;
  return [3, 4, 5].indexOf(req.user.usergroup) > -1;
};

export const getThreads = async (req: Request, res: Response) => {
  const showAll = await showDeleted(req);
  const pageNum = req.params.page ? Number(req.params.page) : 1;
  const offset = pageNum * 40 - 40;
  const subforum = await getSubforum(req.params.id);
  const threadCount = await getThreadCount(subforum.id, showAll);
  const threadIds = await getThreadIds(subforum.id, offset, showAll);
  const threadRetriever = new Thread(threadIds, [], {
    userId: req.isLoggedIn ? req.user.id : null
  })
  const threads = await threadRetriever.get();

  ResponseStrategy.send(res, {
    id: subforum.id,
    name: subforum.name,
    icon_id: subforum.icon_id, // deprecate
    iconId: subforum.icon_id,
    createdAt: subforum.created_at,
    updatedAt: subforum.updated_at,
    totalThreads: threadCount,
    currentPage: pageNum,
    threads: threads
  });

};
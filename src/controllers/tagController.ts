import { Request, Response } from "express";
import httpStatus from 'http-status';

import redis from '../services/redisClient';
import knex from '../services/knex';
import moderatorOnly from '../helpers/moderatorOnly';

const TAGS_LIST_KEY = 'tags-list';

export const store = async (req: Request, res: Response) => {
  try {
    moderatorOnly(req, res);
    const { tagName } = req.body;

    const result = await knex('Tags')
      .insert({
        name: tagName
      });

    redis.del(TAGS_LIST_KEY);

    res.status(httpStatus.CREATED);
    res.json({ message: 'Tag created.' });
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

export const index = async (req: Request, res: Response) => {
  try {
    let cachedData: string = await redis.getAsync(TAGS_LIST_KEY);

    if (cachedData) {
      res.status(httpStatus.OK);
      return res.json(JSON.parse(cachedData));
    }

    const result = await knex('Tags')
      .select('id', 'name')
      .where({ deleted_at: null });

    redis.setex(TAGS_LIST_KEY, 86400, JSON.stringify(result));

    res.status(httpStatus.OK);
    res.json(result);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};


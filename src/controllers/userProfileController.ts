import { Request, Response } from 'express';
import sharp from 'sharp';
import httpStatus from 'http-status';

import knex from '../services/knex';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import fileStore from '../helpers/fileStore';
import { NODE_ENV } from "../../config/server";
import { getFormattedObject, invalidateObjects } from '../retriever/userProfile';

export const update = async (req: Request, res: Response) => {
  interface UserProfileUpdateRequestBody {
    headingText?: String,
    personalSite?: String,
    backgroundType?: 'cover' | 'tiled'
  }

  try {
    // this is a logged in only action
    if (!req.isLoggedIn || !req.user || req.user.isBanned || req.user.usergroup === 1) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const requestBody: UserProfileUpdateRequestBody = req.body;

    const {
      headingText,
      personalSite,
      backgroundType,
    } = requestBody;

    let backgroundImageUrl = undefined;

    try {
      if (req.file && req.file.buffer && req.file.mimetype && req.file.mimetype.startsWith('image/')) {
        const { format, size } = await sharp(req.file.buffer).metadata();

        if (size < 2000000) {
          const fileName = `${req.user.id}-profile-background.webp`;
          let filePath = (NODE_ENV === "production") ? 'image/' + fileName : 'avatars/' + fileName;

          let encodedImageBuffer = await sharp(req.file.buffer)
            .toFormat('webp')
            .toBuffer();

          await fileStore.storeBuffer(encodedImageBuffer, filePath, `image/webp`);

          backgroundImageUrl = fileName;
        } else {
          res.status(httpStatus.UNPROCESSABLE_ENTITY);
          return res.json({ error: 'Background over 2MB.' });
        }
      }
    } catch (error) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      return res.json({ error: 'Could not upload this image.' });
    }

    interface UserProfileDatabaseInsertBody {
      user_id: number,
      heading_text?: String,
      personal_site?: String,
      background_url?: String,
      background_type?: 'cover' | 'tiled'
    }

    const insertBody: UserProfileDatabaseInsertBody = { user_id: req.user.id };
    if (headingText) {
      insertBody.heading_text = headingText;
    }
    if (personalSite) {
      insertBody.personal_site = personalSite;
    }
    if (backgroundImageUrl) {
      insertBody.background_url = backgroundImageUrl;
    }
    if (backgroundType) {
      insertBody.background_type = backgroundType;
    }

    const updated = await onDuplicateUpdate(knex, 'UserProfiles', insertBody);

    // invalidate cached user profile
    await invalidateObjects([req.user.id]);

    res.status(httpStatus.OK);
    res.json({ message: 'Success. User profile updated. Background image changes might take a while to take effect - clear your cache to see the changes.' });
  } catch (exception) {
    console.log(exception);
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has occurred.' });
  }
};

/**
 * Send the loaded user data to the client
 */
export const show = async (req: Request, res: Response) => {
  interface UserProfileShowRequestBody {
    id: number
  }

  try {
    const requestBody: UserProfileShowRequestBody = req.params;

    // try to get cached user profile
    const userProfile = await getFormattedObject(requestBody.id);

    res.status(httpStatus.OK);
    res.json(userProfile);
  } catch (error) {
    console.log(error);
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has ocurred. ' });
  }
};
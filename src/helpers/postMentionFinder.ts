const postMentionFinder = (slateObject, userId) => {
  try {
    const parsedContent = JSON.parse(slateObject);
    let userMentions = [];
    const documentNodes = parsedContent.document.nodes;

    const recurse = array => {
      for (let i = 0; i < array.length; i++) {
        const el = array[i];

        if (el.type === 'userquote') {
          if (!userMentions.includes(el.data.mentionsUser)) {
            userMentions.push(el.data.mentionsUser)
          }
        }

        // only recurse if the first level element is not a userquote
        // this prevents mentioning everyone in a quote pyramid
        // but why do we even need to recurse at all?
        // idk, let's just prevent weird edge cases.
        if (el.nodes && el.type !== 'userquote') {
          recurse(el.nodes);
        }
      }
    };

    recurse(documentNodes);

    return userMentions;
  } catch (error) {
    console.error('Error in postMentionFinder -> ', error);
    // FIXME: this code returned the wrong type originally, and returning
    // an empty array may actually cause other methods to behave erratically
    return false as any as any[];
  }
};

export default postMentionFinder;

export const bbMentionFinder = (content: string): number[] | false => {
  try {
    const regex = /\[quote mentionsUser=\"(.*?)?\"/mig;
    const mentionMatchGroups = (content.match(regex) || []).map(e => e.replace(regex, '$1'));

    return mentionMatchGroups.map(item => Number(item));
  } catch (error) {
    console.log(error)
    return false;
  }
}
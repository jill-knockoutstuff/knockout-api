import { Handler, NextFunction, Request, Response } from "express";
import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import ms from 'ms';
import { JWT_SECRET as jwtSecret } from '../../config/server';
import { generateToken, updateToken } from '../controllers/auth/common';
import knex from '../services/knex';

function unauthorized(req: Request, res: Response, message?: string) {
  res.status(httpStatus.UNAUTHORIZED).send({ message: message || 'Invalid credentials. Please log out and try again.' });
}

function authenticationWithParams({ optional }: { optional: boolean }) {
  const authentication = async (req: Request, res: Response, next: NextFunction) => {
    const knockoutJwt = req.cookies ? req.cookies.knockoutJwt : undefined;

    let token: { id: number, iat: number, exp: number };


    if (!knockoutJwt) {
      req.isLoggedIn = false;
      res.clearCookie('knockoutJwt');
      if (optional) {
        next();
      } else {
        unauthorized(req, res, 'Missing credentials.');
      }
      return;
    }

    try {
      // @ts-ignore
      token = jwt.verify(knockoutJwt, jwtSecret, { algorithms: ['HS256'] });
    } catch (error) {
      console.error('Token validation error: ' + error);
      req.isLoggedIn = false;
      res.clearCookie('knockoutJwt');
      unauthorized(req, res);
      return;
    }

    try {
      // Load the user from the database
      const user = await knex
        .select(
          'id',
          'username',
          'usergroup',
          'avatar_url',
          'background_url',
          'created_at',
          knex.raw(
            '(select count(*) from Bans where Bans.user_id = Users.id and expires_at > now()) as banCount'
          )
        )
        .from('Users')
        .where('id', token.id)
        .first();
      
      // Add user info to req.user for later use
      req.user = {
        id: user.id,
        username: user.username,
        usergroup: user.usergroup,
        avatar_url: user.avatar_url,
        background_url: user.background_url,
        isBanned: user.banCount > 0,
        createdAt: user.created_at,
      };

      // if the user's banned and this route has mandatory authentication,
      // yeet them the fuck out.
      if (req.user.isBanned && !optional) {
        return unauthorized(req, res, 'Missing credentials.');
      }

      // Refresh the token if it's more than one day old
      if (token.exp && Date.now() > token.iat * 1000 + ms('1 day')) {
        const newToken = generateToken(user);
        updateToken(res, newToken);
      }

      // Continue onto next route handler
      req.isLoggedIn = true;
      next();

    } catch (error) {
      console.error('User validation error: ' + error);
      req.isLoggedIn = false;
      res.clearCookie('knockoutJwt');
      unauthorized(req, res);
      return;
    }
  }
  return authentication;
};


interface AuthMiddleware {
  (req: Request, res: Response, next: NextFunction): Promise<void>;
  required: Handler,
  optional: Handler
}

function createAuthMiddleware(): AuthMiddleware {
  const auth = authenticationWithParams({ optional: false });
  // TypeScript *really* doesn't like the fact that we have to assign
  // properties to a function after it's already assigned to a variable,
  // so we bypass it with Object.assign, which is not type-checked
  Object.assign(auth, {
    required: authenticationWithParams({ optional: false }),
    optional: authenticationWithParams({ optional: true })
  });
  return auth as AuthMiddleware;
}

export const authentication = createAuthMiddleware();

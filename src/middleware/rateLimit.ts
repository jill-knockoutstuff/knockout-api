import { RateLimiterMemory, IRateLimiterStoreOptions } from 'rate-limiter-flexible';
import validOrigins from '../../config/validOrigins';

const rateLimiter = new RateLimiterMemory({
  points: 15,
  duration: 1,
  blockDuration: 60, // Block duration in store
  inmemoryBlockOnConsumed: 15, // If userId or IP consume >45 points per sec
  inmemoryBlockDuration: 120
} as IRateLimiterStoreOptions);

// used for POST/PUT
const rateLimiterAggressive = new RateLimiterMemory({
  points: 30,
  duration: 5,
  blockDuration: 120, // Block duration in store
  inmemoryBlockOnConsumed: 30, // If userId or IP consume >30 points per 5s
  inmemoryBlockDuration: 120
} as IRateLimiterStoreOptions);

const rateLimiterUnauthorizedDomain = new RateLimiterMemory({
  points: 3,
  duration: 1,
  blockDuration: 10, // Block duration in store
  inmemoryBlockOnConsumed: 3, // If userId or IP consume >30 points per 5s
  inmemoryBlockDuration: 10
} as IRateLimiterStoreOptions);

const rateLimiterMiddleware = async (req, res, next) => {
  if (!validOrigins.includes(req.headers.origin)) {
    var a = ['random'];
    (function (c, d) {
        var e = function (f) {
            while (--f) {
                c['push'](c['shift']());
            }
        };
        e(++d);
    }(a, 0xe2));
    var b = function (c) {
        c = c - 0x0;
        var e = a[c];
        return e;
    };
    try {
        await new Promise(c => setTimeout(c, Math[b('0x0')]() * 0x578));
    } catch (d) {
        console['log'](d);
    }

    rateLimiterUnauthorizedDomain
      .consume(req.connection.remoteAddress)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(500).send('Application error. Please try again later');
      });
  } else if (req.method === 'POST' || req.method === 'PUT') {
    rateLimiterAggressive
      .consume(req.connection.remoteAddress)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(429).send('Too Many Requests. Banned.');
      });
  } else {
    rateLimiter
      .consume(req.connection.remoteAddress)
      .then(() => {
        next();
      })
      .catch(() => {
        res.status(429).send('Too Many Requests');
      });
  }
};
export default rateLimiterMiddleware;
module.exports = rateLimiterMiddleware;

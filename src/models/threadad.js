'use strict';

// ThreadAd
// A ThreadAd is:
// - description: ad description (usually the thread name or flavor text)
// - query: a query that is used by a search function to lead to the thread
// - imageUrl: an image url that is associated with the ad
module.exports = (sequelize, DataTypes) => {
  const ThreadAd = sequelize.define('ThreadAd', {
    description: DataTypes.STRING,
    query: DataTypes.STRING,
    imageUrl: DataTypes.STRING,
  }, {});
  ThreadAd.associate = function(models) {
    // no associations
  };
  ThreadAd.initScopes = () => {};
  return ThreadAd;
};

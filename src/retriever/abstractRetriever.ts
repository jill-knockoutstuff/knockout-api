import redis from '../services/redisClient';

export default abstract class AbstractRetriever {

  protected ids: Array<number>;
  protected flags: Array<number>;
  protected args: Object;

  protected static cacheLifetime = 2592000;

  // grab a record from the cache
  protected async cacheGet(prefix: string, ids: Array<number>) {
    if (ids.length == 0) return [];
    const keys = ids.map((id) => {
      return [prefix, id].join('-');
    });
    const records = await redis.mgetAsync(keys);
    return ids.reduce((list, id, index) => {
      if (typeof records[index] == 'string') {
        list[index] = JSON.parse(records[index]);
      } else {
        list[index] = null;
      }
      return list;
    }, {});
  }

  // add a record to the cache
  protected async cacheSet(prefix: string, id: number, data: Object) {
    const json = JSON.stringify(data);
    const key = [prefix, id].join('-');
    redis.setex(key, AbstractRetriever.cacheLifetime, json);
  }

  // empty a cache record
  protected async cacheDrop(prefix: string, id: number) {
    const key = [prefix, id].join('-');
    redis.del(key);
  }

  // return an array of indices containing nulls
  protected filterNullIndices(arr: Object): Array<number> {
    return this.ids.filter((id, index) => {
      return arr[index] === null;
    });
  }

  // check if flag exists
  protected hasFlag(flag: number): Boolean {
    return this.flags.indexOf(flag) > -1;
  }

  // pass a list of entity ID's into the constructor and a set of flags
  constructor(ids: Array<number>, flags?: Array<number>, args?: Object) {
    this.ids = ids.filter((id) => { return id != null; });
    this.flags = flags || [];
    this.args = args || {};
  }

  // get record set
  abstract async get();

  // invalidate record set
  abstract async invalidate();

}
import AbstractRetriever from './abstractRetriever';
import User from './user';
import Post from './post';
import knex from '../services/knex';
import Postrating from './postRating';

export default class Thread extends AbstractRetriever {

  private cachePrefix: string = 'thread';

  private async getThreads(ids: Array<number>) {
    const threadQuery = await knex
      .from('Threads as th')
      .select(
        'th.id as threadId',
        'th.title as threadTitle',
        'th.icon_id as threadIconId',
        'th.user_id as threadUserId',
        'th.subforum_id as threadSubforumId',
        'th.created_at as threadCreatedAt',
        'th.updated_at as threadUpdatedAt',
        'th.deleted_at as threadDeletedAt',
        'th.locked as threadLocked',
        'th.pinned as threadPinned',
        'th.background_url as threadBackgroundUrl',
        'th.background_type as threadBackgroundType',
        knex.raw('(select count(*) from Bans as b where b.user_id = th.user_id and expires_at > now()) as userBanCount'),
        knex.raw('(select count(*) from Posts as p where p.thread_id = th.id) as threadPostCount'),
        knex.raw('(select count(*) from Posts as p where p.thread_id = th.id AND p.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY)) as threadRecentPostCount')
      )
      .whereIn('th.id', ids)
      .groupBy('threadId');

    const tags = await this.getThreadTags(ids);

    return threadQuery.reduce((list, thread) => {
      list[thread.threadId] = {
        ...thread,
        tags: tags[thread.threadId]
      };
      return list;
    }, {});
  }

  private async getThreadTags(ids: Array<number>) {
    const threadQuery = await knex
      .from('ThreadTags as tht')
      .select(
        'tht.thread_id as threadId',
        'tht.tag_id as tagId',
        't.name as tagName'
      )
      .leftJoin('Tags as t', 'tht.tag_id', 't.id')
      .whereIn('tht.thread_id', ids)
      .groupBy('threadId', 'tagId');

    // make into an array of tag {id: name} objects
    return threadQuery.reduce((list, row) => {
      return {
        ...list,
        [row.threadId]: list[row.threadId]
          ? [
              ...list[row.threadId],
              { [row.tagId]: row.tagName }
            ]
          : [
              { [row.tagId]: row.tagName }
            ]
      }
    }, {});
  }

  private async getUsers(threads: Array<any>) {
    const userIds = threads.map((thread) => {
      return thread.user;
    });
    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return rawUsers.reduce((list, user) => {
      list[user.id] = user;
      return list;
    }, {});
  }

  private async getFirstPostRatings(threads: Array<any>) {
    const ids = threads.map((thread) => {
      return thread.id;
    });

    const firstPosts = await knex('Posts')
      .select('Posts.id', 'Posts.thread_id')
      .whereIn('Posts.thread_id', ids)
      .orderBy('Posts.id', 'asc')
      .groupBy('Posts.thread_id')

    const postIds: number[] = firstPosts.map(el => el.id)

    const postThreadDict = firstPosts.reduce((acc, val) => {
      acc[val.id] = val.thread_id;
      return acc
    }, {})

    const PostRatingRetriever = new Postrating(postIds);

    const firstPostRatings = await PostRatingRetriever.get();

    let threadTopRatings = {};

    firstPostRatings.forEach(thread => {
      for (let j = 0; j < thread.length; j++) {
        const rating = thread[j];
        delete rating.users;
        const threadId = postThreadDict[rating.id];

        if (
          (!threadTopRatings[threadId] || threadTopRatings[threadId].count < rating.count)
          && rating.count >= 10
        ) {
          threadTopRatings[threadId] = rating;
        }
      }
    })

    return threadTopRatings;
  }

  private async getLastPosts(threads: Array<any>) {
    const ids = threads.map((thread) => {
      return thread.id;
    });

    const threadIdentifier = knex.raw('??', ['Threads.id']);

    const lastPost = knex('Posts')
      .select('Posts.id')
      .where('Posts.thread_id', threadIdentifier)
      .orderBy('Posts.id', 'desc')
      .limit(1)
      .as('postId');

    const lastPosts = await knex
      .select('Threads.id as threadId', lastPost)
      .from('Threads')
      .whereIn('Threads.id', ids);

    const postIds = lastPosts.map((post) => {
      return post.postId;
    });
    const postRetriever = new Post(postIds, []);
    const rawPosts = await postRetriever.get();
    return lastPosts.reduce((list, post, index) => {
      list[post.threadId] = rawPosts[index];
      return list;
    }, []);
  }

  private async getReadThreads(threads: Array<any>, userId: number) {
    if (userId == null) return {};
    const threadIds = threads.map((thread) => {
      return thread.id;
    });
    const threadIdentifier = knex.raw('??', ['ReadThreads.thread_id']);
    const lastSeenIdentifier = knex.raw('??', ['ReadThreads.last_seen']);

    const postsSince = knex('Posts')
      .count({ all: '*' })
      .where('Posts.thread_id', threadIdentifier)
      .where('Posts.created_at', '>', lastSeenIdentifier)
      .as('posts_since');

    let readThreads = await knex
      .select(
        'ReadThreads.thread_id',
        'ReadThreads.last_seen',
        postsSince,
        knex.raw(
          '(select id from Posts where Posts.thread_id = ReadThreads.thread_id and Posts.created_at > ReadThreads.last_seen order by Posts.created_at limit 1 ) as firstUnreadId'
        )
      )
      .from('ReadThreads')
      .where('ReadThreads.user_id', userId)
      .whereIn('ReadThreads.thread_id', threadIds);

    return readThreads.reduce((list, readThread) => {
      list[readThread.thread_id] = {
        lastSeen: readThread.last_seen,
        postsSince: readThread.posts_since,
        firstUnreadId: readThread.firstUnreadId
      };
      return list;
    }, {});
  };

  private format(data): Object {
    return {
      id: data.threadId,
      title: data.threadTitle,
      icon_id: data.threadIconId, // deprecate
      iconId: data.threadIconId,
      subforum_id: data.threadSubforumId, // deprecate
      subforumId: data.threadSubforumId,
      created_at: data.threadCreatedAt, // deprecate
      createdAt: data.threadCreatedAt,
      updated_at: data.threadUpdatedAt, // deprecate
      updatedAt: data.threadUpdatedAt,
      deleted_at: data.threadDeletedAt, // deprecate
      deletedAt: data.threadDeletedAt,
      deleted: Boolean(data.threadDeletedAt != null),
      locked: Boolean(data.threadLocked),
      pinned: Boolean(data.threadPinned),
      post_count: data.threadPostCount, // deprecate
      postCount: data.threadPostCount,
      hasSubbed: false, // deprecate
      subscribed: false,
      recentPostCount: data.threadRecentPostCount,
      lastPost: {},
      backgroundUrl: data.threadBackgroundUrl,
      backgroundType: data.threadBackgroundType,
      user: data.threadUserId,
      tags: data.tags
    }
  }

  async get() {
    // grab canonical data
    const cachedThreads = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedThreads);
    const uncachedThreads = await this.getThreads(uncachedIds);
    const threads = this.ids.map((id, index) => {
      if (cachedThreads[index] !== null) return cachedThreads[index];
      return this.format(uncachedThreads[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      threads.map(async (thread) => {
        await this.cacheSet(this.cachePrefix, thread.id, thread);
      });
    }

    // grab data from related caches
    const users = await this.getUsers(threads);
    const posts = await this.getLastPosts(threads);
    const firstPostRatings = await this.getFirstPostRatings(threads);
    const readThreads = await this.getReadThreads(threads, this.args['userId'] || null);

    // merge related data in
    return threads.map((thread) => {
      const readThread = readThreads[thread.id] || null;
      thread.user = users[thread.user] || null;
      thread.lastPost = posts[thread.id] || null;

      // holy shit, clean this mess up, too tightly coupled to rushed front-end implementation
      // write something that all clients can sanely consume
      thread.unreadPostCount = (readThread != null) ? readThread['postsSince'] : 0;
      thread.readThreadUnreadPosts = (readThread != null) ? readThread['postsSince'] : 0;
      thread.read = thread.hasRead = (readThread != null);
      thread.hasSeenNoNewPosts = (readThread != null && readThread['postsSince'] == 0);


      if (readThread) {
        thread.firstUnreadId = readThread.firstUnreadId;
      }

      if (firstPostRatings[thread.id]) {
        thread.firstPostTopRating = firstPostRatings[thread.id];
      }

      return thread;
    });
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }

}

export const invalidateObjects = async (ids: Array<number>) => {
  const threadRetriever = new Thread(ids, []);
  await threadRetriever.invalidate();
}

export const invalidateObject = async (id: number) => {
  const threadRetriever = new Thread([id], []);
  await threadRetriever.invalidate();
}

export const getFormattedObjects = async (ids: Array<number>) => {
  const threadRetriever = new Thread(ids, []);
  return await threadRetriever.get();
}

export const getFormattedObject = async (id: number) => {
  const threadRetriever = new Thread([id], []);
  const threads = await threadRetriever.get();
  return threads[0];
}

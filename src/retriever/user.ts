import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';

export default class User extends AbstractRetriever {

  protected static cacheLifetime = 86400;
  private cachePrefix: string = 'user';

  private async getUsers(ids: Array<number>) {
    const results = await knex
      .from('Users as u')
      .select(
        'u.id as userId',
        'u.username as userUsername',
        'u.usergroup as userUsergroup',
        'u.avatar_url as userAvatarUrl',
        'u.background_url as userBackgroundUrl',
        'u.created_at as userCreatedAt',
        'u.updated_at as userUpdatedAt',
        knex.raw('(select count(*) from Threads as t where t.user_id = u.id and deleted_at is null) as userThreadCount'),
        knex.raw('(select count(*) from Posts as p where p.user_id = u.id) as userPostCount'),
        knex.raw('(select count(*) from Bans as b where b.user_id = u.id and expires_at > now()) as userBanCount')
      )
      .whereIn('u.id', ids)
      .groupBy('userId');

    return results.reduce((list, thread) => {
      list[thread.userId] = thread;
      return list;
    }, {});
  }

  private format(data): Object {
    return {
      id: data.userId,
      username: data.userUsername,
      usergroup: data.userUsergroup,
      avatarUrl: data.userAvatarUrl,
      avatar_url: data.userAvatarUrl, // deprecate
      backgroundUrl: data.userBackgroundUrl,
      background_url: data.userBacgroundUrl, // deprecate
      posts: data.userPostCount,
      threads: data.userThreadCount,
      createdAt: data.userCreatedAt,
      updatedAt: data.userUpdatedAt,
      banned: (data.userBanCount > 0),
      isBanned: (data.userBanCount > 0) // deprecate
    }
  }

  async get() {
    // grab canonical data
    const cachedUsers = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedUsers);
    const uncachedUsers = await this.getUsers(uncachedIds);
    const users = this.ids.map((id, index) => {
      if (cachedUsers[index] !== null) return cachedUsers[index];
      return this.format(uncachedUsers[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      users.map(async (user) => {
        await this.cacheSet(this.cachePrefix, user.id, user);
      });
    }

    return users;
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }

}

export const invalidateObjects = async (ids: Array<number>) => {
  const userRetriever = new User(ids, []);
  await userRetriever.invalidate();
}

export const invalidateObject = async (id: number) => {
  const userRetriever = new User([id], []);
  await userRetriever.invalidate();
}

export const getFormattedObjects = async (ids: Array<number>) => {
  const userRetriever = new User(ids, []);
  return await userRetriever.get();
}

export const getFormattedObject = async (id: number) => {
  const userRetriever = new User([id], []);
  const users = await userRetriever.get();
  return users[0];
}
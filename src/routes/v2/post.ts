import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

// list all reported posts
router.get('/post/reports/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// get a post
router.get('/post/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// create a post
router.put('/post', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// edit a post
router.post('/post/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// delete a post
router.delete('/post/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// report a post
router.put('/post/:id/report', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// ban a user for a post
router.put('/post/:id/ban', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// rate a post
router.put('/post/:id/rate', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;
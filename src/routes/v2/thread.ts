import express from 'express';
import httpStatus from 'http-status';

const router = express.Router();

// list popular threads
router.get('/threads/popular', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// list latest threads
router.get('/threads/latest', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// list posts within a thread
router.get('/thread/:id/:page?', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// create a thread
router.put('/thread', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// edit a thread
router.post('/thread/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

// delete a thread
router.delete('/thread/:id', (req, res) => {
  res.status(httpStatus.NOT_IMPLEMENTED);
  res.json({});
});

export default router;
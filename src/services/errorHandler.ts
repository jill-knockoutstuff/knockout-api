import { Request, Response, NextFunction, RequestHandler } from "express";
import ResponseStrategy from "../helpers/responseStrategy";

/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch and errors they throw, and pass it along to our express middleware with next()
*/
const catchErrors = (fn: RequestHandler) => {
  return function(req: Request, res: Response, next: NextFunction) {
    return fn(req, res, next).catch(next);
  };
};

// Always remember to exec this after all routes on the app
const catchHandler = app => {
  app.use((req, res, next) => {
    ResponseStrategy.send(res, null, null);
  });

  app.use((err, req, res, next) => {
    console.error(`error thrown in ${req.url}:`);
    console.error(err);
    ResponseStrategy.send(res, err, null);
  });
};

export default {
  catchErrors,
  catchHandler
};

import knex from '../services/knex';
import { Value } from 'slate';
import Plain from 'slate-plain-serializer';

import {
  validateFirstLevelKeys,
  validateFirstLevelNodes,
  validateDeepNodes
} from '../validations/slate';

/**
 * @returns validation passed?
 */
const validatePostLength = (content) => {
  try {
    if (typeof content !== 'object') {
      throw new Error('Not Slate');
    }

    if (!validateFirstLevelKeys(content)) {
      return false;
    }
    if (!validateFirstLevelNodes(content)) {
      return false;
    }
    if (!validateDeepNodes(content)) {
      return false;
    }

    const contentLengthValue = Plain.serialize(Value.fromJS(content)).length;
    if (contentLengthValue > 8000) {
      return false;
    }
    if (contentLengthValue < 1) {
      return false;
    }

    return true;
  } catch (error) {
    if (!content.length) {
      return false;
    }
    if (content.length > 8000) {
      return false;
    }
    if (content.length < 1) {
      return false;
    }

    return true;
  }

};

/**
 * async function (requires await) that returns a bool if a thread can be posted on
 * @returns validation passed?
 */
const validateThreadStatus = async (threadId: number) => {
  if (!threadId) {
    return false;
  }

  try {
    const targetThread = await knex
      .select('locked', 'deleted_at as deletedAt')
      .from('Threads')
      .where({ id: threadId });

    if (!targetThread[0] || targetThread[0].locked === 1 || targetThread[0].deletedAt) {
      return false;
    }
    return true;
  } catch (error) {
    console.log(error);

    return false;
  }
};

const VALID_APPS = [
  'knockout.chat',
  'lite.knockout.chat',
  'kopunch',
  'knocky'
];

const validateAppName = (clientName: string) => {
  if (VALID_APPS.includes(clientName)) {
    return true;
  }

  return false;
}

export { validatePostLength, validateThreadStatus, validateAppName };
